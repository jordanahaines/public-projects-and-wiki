# Public Projects and Wiki

Repo with public projects (that usually accompany blog posts) and wiki (with favorite patterns and practices) for the really cool dude @jordanahaines

## License or rules or whatever
You can literally do whatever you want with whatever you find in this repo. If you think something is cool you _could_ but aren't _required to_ give me a shoutout on [Twitter (@jordanahaines)](https://twitter.com/jordanahaines)
Have fun!