import logging
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet
from schoolusers.serializers import TeacherSerializer, StudentSerializer
from schoolusers.models import Teacher, Student

class TeacherViewset(ModelViewSet):
    # TODO: Firm up these permissions
    permission_classes = (AllowAny,)
    serializer_class = TeacherSerializer
    queryset = Teacher.objects.all()

class StudentViewset(ModelViewSet):
    # TODO: Firm up these permissions
    permission_classes = (AllowAny,)
    serializer_class = StudentSerializer
    queryset = Student.objects.all()

    def perform_create(self, serializer):
        student = serializer.save()
        # Log our new student
        logger = logging.getLogger("watchtower")
        logger.info(f"Student Created: \n\n {student.name} (PK: {student.pk})")