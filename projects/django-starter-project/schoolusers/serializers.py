from rest_framework import serializers
from django.contrib.auth.models import User

from schoolusers.models import Teacher, Student
from schoolclasses.models import SchoolClass

class TeacherSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')
    email = serializers.CharField(source='user.email')
    # Writeable Primary Key Related Field. An array of PKs of classes teacher teaches
    classes = serializers.PrimaryKeyRelatedField(
        queryset=SchoolClass.objects.all(), many=True
    )

    class Meta:
        model = Teacher
        fields = ('pk', 'slug', 'name', 'first_name', 'last_name', 'email', 'salary', 'classes')


class StudentSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')
    email = serializers.CharField(source='user.email')
    # Writeable Primary Key Related Field. An array of PKs of classes teacher teaches
    classes = serializers.PrimaryKeyRelatedField(
        queryset=SchoolClass.objects.all(), many=True, required=False
    )

    class Meta:
        model = Student
        fields = ('pk', 'slug', 'name', 'first_name', 'last_name', 'email', 'classes')
    
    def create(self, validated_data):
        """ Override create to create user and student """
        user_data = validated_data.pop('user')
        user = User.objects.filter(email=user_data['email']).first()
        if not user:
            user = User.objects.create_user(user_data['email'], **user_data)
        validated_data['user'] = user
        return super(StudentSerializer, self).create(validated_data)
