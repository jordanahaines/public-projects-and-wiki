""" This modules contains all of our users (who in turn have a relation to Django's auth.user)
"""
# pylint: disable=missing-function-docstring

from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from demoschool.models import BaseModel

def get_default_grad_year():
    # Function to get our default graduation year -- four years from now
    return timezone.now().year

class Student(BaseModel):
    """ A student participates in classes and submites homework """
    user = models.OneToOneField('auth.user', related_name='student', on_delete=models.CASCADE)
    graduation_year = models.SmallIntegerField(default=get_default_grad_year)

    """ Incoming FK """
    # classes > many SchoolClass

    @property
    def name(self):
        # pylint: disable=E1101
        return self.user.get_full_name()

    def __str__(self):
        return self.name

class Teacher(BaseModel):
    """ A teacher teaches classes """
    user = models.OneToOneField('auth.user', related_name='teacher', on_delete=models.CASCADE)
    salary = models.DecimalField(max_digits=9, decimal_places=2, default=75000)

    """ Incoming FK """
    # classes > many SchoolClass

    @property
    def name(self):
        # pylint: disable=E1101
        return self.user.get_full_name()

    def __str__(self):
        return self.name
    