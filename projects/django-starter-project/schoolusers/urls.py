
from rest_framework.routers import DefaultRouter
from schoolusers.views import StudentViewset, TeacherViewset
# All patterns prefaced by /user/
# pylint: disable=invalid-name
app_name = 'schoolusers'
router = DefaultRouter()
router.register('students', StudentViewset, basename='students')
router.register('teachers', TeacherViewset, basename='teachers')


urlpatterns = [
] + router.urls