""" This module contains abstract base class models that other models inherit from
    Basically, there are a few fields that we want on many of our models, and we inherit
    from models here to make that happen
"""
from uuid import uuid4
from django.db import models

class BaseModel(models.Model):
    """ Base model that, when inherited from, adds a few useful fields """
    # I like to refrain from exposing primary keys (numeric IDs) to users. Slugs are a more secure way to
    # identify individual objects
    slug = models.UUIDField(default=uuid4, editable=False)
    # When the object was created
    created = models.DateTimeField(auto_now_add=True)
    # When the object was last updated
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
    