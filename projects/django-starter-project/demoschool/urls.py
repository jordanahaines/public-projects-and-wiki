"""djangostarter URL Configuration
"""
from django.urls import path, include
from django.contrib import admin
from django.conf.urls.static import static

from django.conf import settings

# pylint: disable=invalid-name
urlpatterns = [
    path("admin/", admin.site.urls),
    path("grappelli/", include("grappelli.urls")),

    # For use with django admin hijack: https://github.com/arteria/django-hijack-admin
    # I use this often but haven't included it in this base projct
    # path("hijack/", include("hijack.urls", namespace="hijack")),

    # We include URLs from each of our apps
    path("user/", include("schoolusers.urls", namespace="user")),
    path("class/", include("schoolclasses.urls", namespace="class")),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
