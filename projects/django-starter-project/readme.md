# Django Starter Project
This directory represents a starting place for Django projects. This project is a super duper basic LMS (learning
management system) for a single class with some students and assignments. I copy and extend this project to quickly
create new projects for everything from illustrating points in blog posts to spinning up new side projects.
Like all of my Django projects, this project uses:
- [Django Rest Framework](https://www.django-rest-framework.org/)
- [Django Nose Test Runner](https://github.com/jazzband/django-nose)
- [Django Extensions for great utilities that make your life much better](https://django-extensions.readthedocs.io/en/latest/)

(Reminder to self: virtualenv `django-starter`)