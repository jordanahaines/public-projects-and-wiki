from rest_framework import serializers
from schoolclasses.models import SchoolClass
from schoolusers.models import Teacher, Student

class SchoolClassSerializer(serializers.ModelSerializer):
    students = serializers.PrimaryKeyRelatedField(
        queryset=Student.objects.all(), many=True
    )
    teacher = serializers.PrimaryKeyRelatedField(
        queryset=Teacher.objects.all()
    )

    class Meta:
        model = SchoolClass
        fields = ('pk', 'slug', 'name', 'students', 'teacher')