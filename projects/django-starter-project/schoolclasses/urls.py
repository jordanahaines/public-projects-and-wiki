
from rest_framework.routers import DefaultRouter
from schoolclasses.views import SchoolClassViewset

# pylint: disable=invalid-name
app_name = 'schoolclasses'
router = DefaultRouter()
router.register('school-classes', SchoolClassViewset, basename="school_classes")


urlpatterns = [
] + router.urls