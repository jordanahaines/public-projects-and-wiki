from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny
from schoolclasses.models import SchoolClass
from schoolclasses.serializers import SchoolClassSerializer

class SchoolClassViewset(ModelViewSet):
    # TODO: Implement stricter permissions
    permissions = (AllowAny,)
    queryset = SchoolClass.objects.all()
    serializer_class = SchoolClassSerializer