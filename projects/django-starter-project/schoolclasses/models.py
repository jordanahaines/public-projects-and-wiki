# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from demoschool.models import BaseModel

class SchoolClass(BaseModel):
    """ A class that a single teacher teaches and multiple students can be enrolled in """
    name = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    teacher = models.ForeignKey('schoolusers.Teacher', related_name='classes', null=True, blank=True, on_delete=models.SET_NULL)
    students = models.ManyToManyField('schoolusers.Student', related_name='classes', blank=True)
    # Nothing fancy - we assume that classes happen at the same time every day and no other scheduling information
    # is necessary
    start_time = models.TimeField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)

    def __str__(self):
        return self.name
